# frozen_string_literal: true

# Version
module CMSScanner
  VERSION = '0.12.0'
end
